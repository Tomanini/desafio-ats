<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $fillable = [
        'id',
        'name',
        'abbr',
    ];

    public function cities()
    {
        return $this->hasMany(CityEntity::class, 'state_id', 'id');
    }
}
