
<footer class="page-footer font-small blue pt-4">
    <div class="col-md-6 mt-md-0 mt-3">
        <h5 class="text-uppercase">Informações de login:</h5>
        <p>user: teste@teste.com</p>
        <p>senha: 123456</p>
      </div>
      <hr>
      <div class="col-md-6 mb-md-0 mb-3">
        <h5 class="text-uppercase">rotas gerais de login: </h5>

        <ul class="list-unstyled">
          <li>
            <p>/auth/login </p>
          </li>
          <li>
            <p>/auth/logout</p>
          </li>
          <li>
            <p>/auth/register</p>
          </li>
        </ul>
      </div>
</footer>