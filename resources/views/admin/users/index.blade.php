
@extends('layout')

@section('content')
<div class="card">
    <article class="card-body">
        <aside class="col-sm-8">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Foto</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Nascimento</th>
                    <th scope="col">Sexo</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">CEP</th>
                    <th scope="col">Cidade</th>
                    <th scope="col">Estado</th>
                </tr>
                </thead>
                <tbody>
            @foreach($users AS $user)
                <tr>
                    <th scope="col">{{ $user->id }}</th>
                    <td>{{ $user->photo }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->birthday }}</td>
                    <td>{{ $user->gender }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ $user->zipcode }}</td>
                    <td>{{ $user->city->name }}</td>
                    <td>{{ $user->state->name }}</td>
                </tr>
            @endforeach
                </tbody>
            </table>

        </aside>
    </article>
@endsection('content')