@extends('layout')

@section('content')
<aside class="col-sm-4">
<div class="card">
<article class="card-body">
	<h4 class="card-title text-center mb-4 mt-1">Login</h4>
	<hr>
	<p class="text-success text-center">As informações de login estão no rodapé</p>
    <form method="POST" action="/auth/login">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    
        <div class="form-group">
            <label class="col-md-4 control-label">Email</label>
            <input type="email" name="email" value="{{ old('email') }}" class="form-control">
        </div>
    
        <div class="form-group">
            <label class="col-md-4 control-label">Senha</label>
            <input type="password" name="password" id="password" class="form-control">
        </div>
    
        <div>
            <input type="checkbox" name="remember">Lembrar
        </div>
    
        <div>
            <button type="submit">Logar</button>
        </div>
    </form>
</article>
</div>
</aside>


@extends('footer')
