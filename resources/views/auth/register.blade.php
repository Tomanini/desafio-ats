
@extends('layout')

@section('content')
<aside class="col-sm-4">
<div class="card">
<article class="card-body">
	<h4 class="card-title text-center mb-4 mt-1">Cadastrar usuário</h4>
	<hr>
	<p class="text-success text-center">As informações complementares estão no rodapé</p>

<form method="POST" action="/auth/register">
    {!! csrf_field() !!}

    <div class="form-group">
        <label class="col-md-4 control-label">Nome</label>
        <input type="text" name="name" value="{{ old('name') }}" class="form-control">
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Email</label>
        <input type="email" name="email" value="{{ old('email') }}" class="form-control">
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Senha</label>
        <input type="password" name="password" class="form-control">
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Confirma senha</label>
        <input type="password" name="password_confirmation" class="form-control">
    </div>

    <div>
        <button type="submit">Register</button>
    </div>
</form>